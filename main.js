function newRowSoftware(app) {
    // check for empty fields, if so fill them with a 'space'
    for(var field in app) {
        if(app.hasOwnProperty(field)) {
            if(app[field] == "") {
                app[field] = "&nbsp;";
            }
        }
    }

    var newRow = document.createElement("div");

    switch(app.name) {
    case "Uptime":
        newRow.setAttribute("class", "table_row special");
        newRow.setAttribute("title", "Time since last reboot"); // tooltip

        var newCell1 = document.createElement("div");
        newCell1.setAttribute("class", "table_left width_30");
        newCell1.innerHTML = app.name;
        newRow.appendChild(newCell1);

        var newCell2 = document.createElement("div");
        newCell2.setAttribute("class", "table_right width_70");
        newCell2.setAttribute("id", "uptime-field");
        newCell2.innerHTML = convertSeconds(app.description);
        newRow.appendChild(newCell2);

        break;
    default:
        newRow.setAttribute("class", "table_row");

        var newCell1 = document.createElement("div");
        newCell1.setAttribute("class", "table_left width_30");
        newCell1.innerHTML = app.name;
        newRow.appendChild(newCell1);

        var newCell2 = document.createElement("div");
        newCell2.setAttribute("class", "table_left width_20");
        newCell2.innerHTML = app.version;
        newRow.appendChild(newCell2);

        var newCell3 = document.createElement("div");
        newCell3.setAttribute("class", "table_left width_10");
        // set colors for online and offline applications respectively
        if(app.status == "Online") { newCell3.setAttribute("style", "color: #2ecc71;"); }
        if(app.status == "Offline") { newCell3.setAttribute("style", "color: #c0392b;"); }
        newCell3.innerHTML = app.status;
        newRow.appendChild(newCell3);

        var newCell4 = document.createElement("div");
        newCell4.setAttribute("class", "table_right width_40");
        newCell4.innerHTML = app.description;
        newRow.appendChild(newCell4);

        break;
    }

    document.getElementById("software_table_wrapper").appendChild(newRow);
}

// takes seconds as an argument and returns them in a human readable way
function convertSeconds(seconds) {
    var output = "";

    years = seconds/31557600;
    if (years >= 1) {
        years = Math.floor(years);
        seconds %= 3153600;
        output += years;
        if(years == 1) {
            output += " year, ";
        } else {
            output += " years, ";
        }
    }

    days = seconds/86400;
    if (days >= 1) {
        days = Math.floor(days);
        seconds %= 86400;
        output += days;
        if(days == 1) {
            output += " day, ";
        } else {
            output += " days, ";
        }
    }

    hours = seconds/3600;
    if(hours >= 1) {
        hours = Math.floor(hours);
        seconds %= 3600;
        output += hours;
        if(hours == 1) {
            output += " hour and ";
        } else {
            output += " hours and ";
        }
    }

    minutes = seconds/60;
    if(minutes >= 1) {
        minutes = Math.floor(minutes);
        seconds %= 60;
        output += minutes;
        if(minutes == 1) {
            output += " minute";
        } else {
            output += " minutes";
        }
    }

    return output;
}

function newSectionNews(news) {
    var newSection = document.createElement("section");
    newSection.innerHTML = "<h3>" + news.date + "</h3>" + news.content;
    document.getElementById("news").appendChild(newSection);
}

function requestSoftware() {
    document.getElementById("software-loader").style.display = "block";
    document.getElementById("software_table_wrapper").style.display = "none";

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if(request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.response);
            for(var i = 0; i < data.length; i++) {
                newRowSoftware( data[i] );
            }

            document.getElementById("software-loader").style.display = "none";
            document.getElementById("software_table_wrapper").style.display = "block";
        }
    };
    request.open("GET", "software.json", true);
    request.send();
}

function requestNews() {
    document.getElementById("news-loader").style.display = "block";

    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if(request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.response);
            for(var i = 0; i < data.length; i++) {
                newSectionNews( data[i] );
            }

            document.getElementById("news-loader").style.display = "none";
        }
    };
    request.open("GET", "news.json", true);
    request.send();
}

window.onload = function (){
    requestNews();
    requestSoftware();
};
