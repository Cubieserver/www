package main

import (
	"bufio"         // buffered Input/Output
	"encoding/json" // for encoding the output in JSON
	"fmt"           // for printing output
	"net/http"      // for HTTP support
	"net/http/cgi"  // for CGI support
	"os"            // for reading and writing files from the OS
	"os/exec"       // for running commands on the system
	"strings"       // for string manipulation
)

type Application struct {
	Name        string `json:"name"`
	Version     string `json:"version"`
	Status      string `json:"status"`
	Description string `json:"description"`
}

func response(w http.ResponseWriter, r *http.Request) {
	//func response() {
	data := []Application{}

	// KERNEL & OS
	data = append(data, *checkLinux())
	data = append(data, *checkOS())

	// WEB
	data = append(data, *checkNginx())
	data = append(data, *checkPHP())

	// DATABASE
	data = append(data, *checkMySQL())
	data = append(data, *checkMemcached())

	// CHAT
	data = append(data, *checkIRCD())
	data = append(data, *checkZNC())
	data = append(data, *checkProsody())
	data = append(data, *checkWebchat())

	// MISC
	data = append(data, *checkTransmission())
	data = append(data, *checkUptime())

	json, _ := json.Marshal(data)

	//fmt.Println(string(json))
	fmt.Fprintf(w, string(json))

	// Show length and capacity of Slice 'data'
	// fmt.Println(len(data), cap(data))
}

func main() {
	http.HandleFunc("/", response)
	cgi.Serve(nil)
	//response()
}

// This function checks the status of a Service reported by systemd
// takes application as input and outputs true for active and false for !active
func isActive(application string) (status bool) {
	// call systemd helper 'systemctl' and report only the property 'ActiveState'
	command := exec.Command("systemctl", "show", application, "-p", "ActiveState")
	commandRaw, error := command.Output()

	if error != nil {
		return false
	}

	// Check if the ActiveState is 'active' and return appropriate value
	if strings.HasSuffix(string(commandRaw), "=active\n") {
		return true
	} else {
		return false
	}

}

// This function reads a file from the specified path and return its lines in a slice
// as well as any errors
func readLines(path string) (lines []string, err error) {
	// Open file and check for any errors
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	// close File when function exits
	defer file.Close()

	// let 'scanner' read the file and put each line into the slice
	// lines [] string was already specified in the header
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	// return the slice and (possible) errors from scanner
	return lines, scanner.Err()
}
