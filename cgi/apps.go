package main

import (
	"os/exec" // for running commands on the system
	"strings" // for manipulating strings
)

// EMPTY EXAMPLE IMPLEMENTATION
// function name consists of "check" + Application Name
// function returns pointer to the create Application{} struct
// func checkXXX() *Application {
// 	xxx := Application{}

// 	// NAME
// 	xxx.Name = "XXX"

// 	// VERSION
// 	xxx.Version = "x.x"

// 	// STATUS
// 	xxx.Status = "Online/Offline/Maintenance"

// 	// DESCRIPTION
// 	xxx.Description = "Description"

// 	return &xxx
// }

func checkNginx() *Application {
	nginx := Application{}

	// VERSION
	command := exec.Command("/usr/sbin/nginx", "-v")
	commandRaw, err := command.CombinedOutput()

	if err == nil {
		version := strings.Split(string(commandRaw), "/")
		nginx.Version = strings.TrimRight(string(version[1]), "\n")
	}

	// NAME
	nginx.Name = "Nginx"

	// STATUS
	if isActive("nginx") {
		nginx.Status = "Online"
	} else {
		nginx.Status = "Offline"
	}

	// DESCRIPTION
	nginx.Description = "Web Server"

	return &nginx
}

func checkUptime() *Application {
	uptime := Application{}

	// NAME
	uptime.Name = "Uptime"

	// DESCRIPTION (== value)
	lines, err := readLines("/proc/uptime")

	if err == nil {
		up := strings.Split(lines[0], ".")
		uptime.Description = up[0]
	}

	return &uptime
}

func checkMySQL() *Application {
	mysql := Application{}

	// Check MySQL Version
	command := exec.Command("mysql", "-V")
	commandRaw, err := command.Output()

	if err == nil {
		version := strings.Split(string(commandRaw), ",")
		version = strings.Split(version[0], "  ")
		mysql.Version = string(version[1])
	}

	// MySQL name
	mysql.Name = "MySQL"

	// Check MySQL Status
	if isActive("mysql") {
		mysql.Status = "Online"
	} else {
		mysql.Status = "Offline"
	}

	// Nginx description
	mysql.Description = "SQL Database Server"

	return &mysql
}

func checkLinux() *Application {
	linux := Application{}

	// Check Linux Version
	command := exec.Command("uname", "-r")
	commandRaw, err := command.Output()

	if err == nil {
		linux.Version = strings.TrimRight(string(commandRaw), "\n")
		// Gotta remove those trailing line breaks
	}

	// Linux name
	linux.Name = "Linux" // We are only talking about the Kernel ...

	// Check Linux Status
	linux.Status = "Online" // Obviously Online ...

	// Linux description
	linux.Description = "Kernel"

	return &linux
}

func checkOS() *Application {
	os := Application{}

	// Check Debian Version
	lines, err := readLines("/etc/debian_version")

	if err == nil {
		os.Version = string(lines[0])
	}

	// OS Name
	os.Name = "Debian GNU/Linux"

	// Check Os Status
	os.Status = "Online" // Obviously Online ...

	// Os description
	os.Description = "Operating System"

	return &os
}

func checkIRCD() *Application {
	ircd := Application{}

	// VERSION
	command := exec.Command("/usr/sbin/ircd-hybrid", "-version")
	commandRaw, err := command.Output()

	if err == nil {
		version := strings.Split(string(commandRaw), ":")
		version = strings.Split(string(version[2]), "+")

		ircd.Version = string(version[0])
	}

	// NAME
	ircd.Name = "ircd-hybrid"

	// STATUS
	if isActive("ircd-hybrid") {
		ircd.Status = "Online"
	} else {
		ircd.Status = "Offline"
	}

	// DESCRIPTION
	ircd.Description = "IRC Server"

	return &ircd
}

func checkZNC() *Application {
	znc := Application{}

	// VERSION
	command := exec.Command("znc", "--version")
	commandRaw, err := command.Output()

	if err == nil {
		version := strings.Split(string(commandRaw), " - ")
		znc.Version = string(version[1])
	}

	// NAME
	znc.Name = "ZNC"

	// STATUS
	if isActive("znc") {
		znc.Status = "Online"
	} else {
		znc.Status = "Offline"
	}

	// DESCRIPTION
	znc.Description = "IRC Bouncer"

	return &znc
}

func checkPHP() *Application {
	php := Application{}

	// VERSION
	command := exec.Command("php", "--version")
	commandRaw, err := command.Output()

	if err == nil {
		version := strings.Split(string(commandRaw), " ")
		php.Version = string(version[1])
	}

	// NAME
	php.Name = "PHP5"

	// STATUS
	if isActive("php5-fpm") {
		php.Status = "Online"
	} else {
		php.Status = "Offline"
	}

	// DESCRIPTION
	php.Description = "Scripting language run by Fast Process Manager (PHP5-FPM)"

	return &php
}

func checkMemcached() *Application {
	mem := Application{}

	// VERSION
	command := exec.Command("memcached", "-V")
	commandRaw, err := command.Output()

	if err == nil {
		version := strings.Split(string(commandRaw), " ")
		mem.Version = strings.TrimRight(string(version[1]), "\n")
	}

	// NAME
	mem.Name = "Memcached"

	// STATUS
	if isActive("memcached") {
		mem.Status = "Online"
	} else {
		mem.Status = "Offline"
	}

	// DESCRIPTION
	mem.Description = "Memory Caching System"

	return &mem
}

func checkTransmission() *Application {
	transmission := Application{}

	// VERSION
	command := exec.Command("transmission-daemon", "-V")
	commandRaw, err := command.CombinedOutput()

	if err == nil {
		version := strings.Split(string(commandRaw), " ")
		transmission.Version = string(version[1])
	}

	// NAME
	transmission.Name = "Transmission"

	// STATUS
	if isActive("transmission-daemon") {
		transmission.Status = "Online"
	} else {
		transmission.Status = "Offline"
	}

	// DESCRIPTION
	transmission.Description = "Torrent Client and Server"

	return &transmission
}

func checkProsody() *Application {
	prosody := Application{}

	// VERSION
	// command := exec.Command("prosody", "--version")
	// commandRaw, err := command.Output()

	// if err == nil {
	//	version := strings.Split(string(commandRaw), " ")
	//	prosody.Version = string(version[1])
	// }

	// NAME
	prosody.Name = "Prosody"

	// STATUS
	if isActive("prosody") {
		prosody.Status = "Online"
	} else {
		prosody.Status = "Offline"
	}

	// DESCRIPTION
	prosody.Description = "XMPP-/Jabber Server"

	return &prosody
}

func checkWebchat() *Application {
	webchat := Application{}

	// VERSION
	// command := exec.Command("webchat", "--version")
	// commandRaw, err := command.Output()

	// if err == nil {
	// 	version := strings.Split(string(commandRaw), " ")
	// 	webchat.Version = string(version[1])
	// }

	// NAME
	webchat.Name = "qWebIRC"

	// STATUS
	// if isActive("prosody") {
	// 	webchat.Status = "Online"
	// } else {
	// 	webchat.Status = "Offline"
	// }

	// DESCRIPTION
	webchat.Description = "Web Client for IRC"

	return &webchat
}
